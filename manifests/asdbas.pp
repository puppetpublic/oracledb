# AS *only* DBA config

class oracledb::asdbas {

  include oracledb::as
  include oracledb::k5login::as
  include user::emagent

  base::iptables::rule { 'as_dba_access':
    description => 'AS VDI range for DBAs to access these systems',
    source      => ['172.20.200.0/23'],
    port        => ['1533','1534','1535'],
    protocol    => 'tcp',
  }

}
