# additional directories usually needed
class oracledb::directories {
  file {
    [ '/var/opt/oracle',
      '/u02',
      '/u03',
      '/u01/app/rman', ]:
        ensure => directory,
        owner  => 'oracle',
        group  => 'dba',
        mode   => '0770';
  }
}
