# Ntirety DBA user list for .k5login files (oracle and ntirety users)

class oracledb::ntirety {

  $ntiretyusers = [
                   'cgarcia5@stanford.edu',
                   'chapmci1@stanford.edu',
                   'dcampoy@stanford.edu',
                   'djeanbap@stanford.edu',
                   'dwinsor@stanford.edu',
                   'fabianek@stanford.edu',
                   'hodgkins@stanford.edu',
                   'jccorey@stanford.edu',
                   'jgoode2@stanford.edu',
                   #'jhaas1@stanford.edu', # win only
                   'kalis@stanford.edu',
                   'ltalaric@stanford.edu',
                   'jgims@stanford.edu',
                   'jhussey@stanford.edu',
                   'johnr2@stanford.edu',
                   'nstha78@stanford.edu',
                   #'pablos@stanford.edu', # win only
                   'pgali@stanford.edu',
                   'scosta3@stanford.edu',
                   #'terriew@stanfor.edu', # win only
                   'tiffanyv@stanford.edu',
                   'vkalyana@stanford.edu',
                   #'wsheffie@stanford.edu', # win only
                  ]

}
