# oracledb packages
class oracledb::packages {
  package {
    [ 'binutils',
      'control-center',
      'gcc',
      'gcc-c++',
      'glibc',
      'glibc-common',
      'glibc-devel',
      'glibc-headers',
      'libgcc',
      'libaio',
      'make',
      'sysstat',
      'xorg-x11-xauth', ]:
        ensure => present;
    }
  ## TDPO rpms exist only for el4 and 5. Only BIN exists for el6, so install
  ## by hand.
  case $::lsbmajdistrelease  {
    '4','5' : {
      package { [
        'stanford-TDPO',
        'TDP-Oracle',
        'TDP-Oracle.Utility'
        ]:
          ensure => present,
      }
    }
    # rhel6 will give you an error if tdp BIN file not installed
    default: {
      exec { 'check el6 tdp bin installed':
        command   => 'echo TDP is not installed! Install it by hand from \
                      /afs/ir/product/storage/TDPO_LINUX_LATEST',
        logoutput => true,
        unless    => 'ls /opt/tivoli/tsm/client/oracle/properties/version/TSM_Data_Protection_Oracle.cmptag',
      }
    }
  }
}

class oracledb::packages::no_tdp inherits oracledb::packages {

  Exec['check el6 tdp bin installed'] {
    command   => 'echo you probably want the oracledb::packages class instead of \
                  oracledb::packages::no_tdp, or you should uninstall TDP',
    logoutput => true,
    onlyif    => 'ls /opt/tivoli/tsm/client/oracle/properties/version/TSM_Data_Protection_Oracle.cmptag',
  }

}
