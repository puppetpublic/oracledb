# oracle users
class oracledb::oracleusers {

  # Standard sudoers file for Oracle DBAs.
  # TODO: long term strategy is to use /etc/sudoers.d fragments
  base::textline {
    '%dba    ALL = NOPASSWD: /usr/bin/dsmc q se':
      ensure       => '/etc/sudoers';
    'oracle  ALL= NOPASSWD: /usr/bin/tdpoconf PassWord *':
      ensure       => '/etc/sudoers',
      escaped_name => 'oracle  ALL= NOPASSWD: /usr/bin/tdpoconf PassWord \*';
  }

}
