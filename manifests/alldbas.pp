# class to handle oracle user .k5login for both AS and Ntirety admins

class oracledb::alldbas {

  include oracledb::as
  include oracledb::k5login::all
  include oracledb::ntirety
  include user::emagent
  include user::ntirety

}
