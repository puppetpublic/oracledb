# AS DBA user list for .k5login files (oracle and emagent users)

class oracledb::as {

  $asusers = [
              'jenhong@stanford.edu',
              'kmadduri@stanford.edu',
              'sanjeevk@stanford.edu',
              'toaivo@stanford.edu',
             ]

}
