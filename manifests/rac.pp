#  Oracle RAC setup
# TODO: look at using a define to solve some of these deps
# TODO: does this need to modify nsswitch.conf to set hosts: dns files?

# REQUIRES:
## hosts file entries for dns names, pvt, and vip
######
## ssh setup files
# file {
#   '/u01/app/oracle/.ssh/authorized_keys':
#            ensure   => file,
#            source   => "puppet:///modules/CLIENT/u01/app/oracle/dotssh/authorized_keys.${hostname}",
#            mode     => 640,
#            owner    => oracle,
#            require  => File['/u01/app/oracle/.ssh'];
#         '/u01/app/oracle/.ssh/id_rsa.pub':
#            ensure   => file,
#            source   => "puppet:///modules/CLIENT/u01/app/oracle/dotssh/id_rsa.pub.${hostname}",
#            mode     => 640,
#            owner    => oracle,
#            require  => File['/u01/app/oracle/.ssh'];
#
#   '/u01/11.2.0/grid/.ssh/authorized_keys':
#      ensure   => file,
#      source   => "puppet:///modules/CLIENT/u01/11.2.0/grid/dotssh/authorized_keys.${hostname}",
#      mode     => 640,
#      owner    => grid,
#      require  => File['/u01/11.2.0/grid/.ssh'];
#   '/u01/11.2.0/grid/.ssh/id_rsa.pub':
#      ensure   => file,
#      source   => "puppet:///modules/CLIENT/u01/11.2.0/grid/dotssh/id_rsa.pub.${hostname}",
#      mode     => 640,
#      owner    => grid,
#      require  => File['/u01/11.2.0/grid/.ssh'];
# }
#######
## wallet setup
#   base::wallet { "unix-${hostname}-grid-ssh-rsa":
#    path    => '/u01/11.2.0/grid/.ssh/id_rsa',
#    owner   => grid,
#    group   => oinstall,
#    type    => "file",
#    mode    => 600,
#    require => File['/u01/11.2.0/grid/.ssh'];
#  }
#  base::wallet { "unix-${hostname}-oracle-ssh-rsa":
#    path    => '/u01/app/oracle/.ssh/id_rsa',
#    owner   => oracle,
#    group   => oinstall,
#    type    => 'file',
#    mode    => 600,
#    require => File['/u01/app/oracle/.ssh'];
#  }
## bonding setup for NICs to setup LACP mode 1 on nic pairs
# file { '/etc/modprobe.d/bonding.conf':
#   source => 'puppet:///modules/CLIENT/etc/modprobe.d/bonding.conf',
# }


class oracledb::rac inherits oracledb::rhel5 {
  include oracledb::rac::newsyslog
  include oracledb::rac::ntirety
  include oracledb::rac::ntpd
  include oracledb::rac::root
  include oracledb::rac::users
  include oracledb::rac::v11g
  include powerpath
  include base::ssh::pubkey

  if (! ( $::operatingsystem == 'redhat' and $::lsbmajdistrelease == '5' )) {
    fail 'Must be running RHEL5 to use RAC'
  }

  # Oracle modules need to be rewritten!!! This is hacked to work around it.
  if ! defined (Package['libaio-devel.i386']) {
    package { 'libaio-devel.i386': ensure => present }
  }

  if ! defined (Package['libaio-devel.x86_64']) {
    package { 'libaio-devel.x86_64': ensure => present }
  }

  if ! defined (Package['unixODBC.i386']) {
    package { 'unixODBC.i386': ensure => present }
  }

  if ! defined (Package['unixODBC.x86_64']) {
    package { 'unixODBC.x86_64': ensure => present }
  }

  if ! defined (Package['unixODBC-devel.i386']) {
    package { 'unixODBC-devel.i386': ensure => present }
  }

  if ! defined (Package['compat-libstdc++-33.x86_64']) {
    package { 'compat-libstdc++-33.x86_64': ensure => present }
  }
  
  package { [ 'elfutils-libelf-devel.x86_64', 
              'unixODBC-devel.x86_64',
            ]:
    ensure => present
  }

  File['/etc/security/limits.conf'] {
    source => 'puppet:///modules/oracledb/etc/security/limits.conf'
  }

  file { '/etc/oraInst.loc':
    source  => 'puppet:///modules/oracledb/etc/oraInst.loc',
    require => User['oracle'],
    owner   => 'root',
    mode    => '0644';
  }

  # link in sqlplus to /usr/local/bin
  file { '/usr/local/bin/sqlplus':
    ensure  => link,
    target  => '/u01/app/oracle/product/10.2.0/db_1/bin/sqlplus';
  }

  # oracle and dba group needs access to powerpath devices
  exec { 'chown oracle:dba /dev/emcpower*':
    command => 'chown oracle:dba /dev/emcpower* && chmod 660 /dev/emcpower*',
    onlyif  => 'ls -l /dev/emcpower* | egrep -q \'root|disk|brw-r-\'',
    require => Package['EMCpower.LINUX'],
  }

  # We need Oracle ASM for currently booted kernel and other req oracle pkgs.
  package {
    [ 'cvuqdisk',
      'oracleasm-support',
      "oracleasm-${::kernelrelease}",
      'oracleasmlib', ]:
        ensure => present;
  }

  # Start oracleasm service after the oracleasm kernel package is installed.
  service { 'oracleasm':
    ensure  => running,
    enable  => true,
    status  => '/sbin/service oracleasm status | grep -q yes',
    require => Package["oracleasm-${::kernelrelease}"],
  }

}

# override ntp config file to use -x flag
class oracledb::rac::ntpd inherits base::ntp {
  File['/etc/sysconfig/ntpd'] {
    source => 'puppet:///modules/oracledb/etc/sysconfig/ntpd',
  }
}

class oracledb::rac::ntirety inherits user::ntirety {
  include group::asmadmin
  include group::asmdba
  include group::asmoper
  include group::oinstall

  User['ntirety']  {
    groups => ['asmadmin','asmdba','asmoper','oinstall'],
  }
}

class oracledb::rac::oracle-user inherits user::oracle {
    include group::oinstall
    include group::asmdba

    User['oracle']  {
        gid     => 'dba',
        groups  => ['oinstall','asmdba'],
        require => [ Group['dba'], Group['asmdba'], Group['oinstall'] ],
    }

    File['/u01/app/oracle'] {
            mode     => '0640',
    }

    file {
        '/u01/app/oracle/.ssh':
            ensure   => directory,
            mode     => '0640',
            owner    => 'oracle',
            require  => [ User['oracle'], File['/u01/app/oracle'] ];
        '/u02/app':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02'] ];
        '/u02/app/oracle':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02/app'] ];
        '/u02/app/oracle/cfgtoollogs':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02/app/oracle'] ];
        '/u02/app/oracle/product':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02/app/oracle'] ];
        '/u02/app/oracle/product/11.2.0':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02/app/oracle/product'] ];
        '/u02/app/oracle/product/11.2.0/db_1':
            ensure   => directory,
            mode     => '0664',
            owner    => 'oracle',
            group    => 'oinstall',
            require  => [ Group['oinstall'], User['oracle'], File['/u02/app/oracle/product/11.2.0'] ];
    }
}

# add grid to dba group
class oracledb::rac::grid-user inherits user::grid {
  include group::dba

  User['grid'] {
    gid => 'dba',
  }
}

class oracledb::rac::users inherits user::virtual {
  include oracledb::ntirety
  include oracledb::rac::ntirety
  include oracledb::rac::v11g
  include oracledb::rac::grid-user
  include oracledb::rac::oracle-user

  file {
    '/etc/profile.d/oraclegrid.sh':
      ensure   => file,
      source   => 'puppet:///modules/oracledb/etc/profile.d/oraclegrid.sh',
      mode     => '0755',
      owner    => 'root',
      group    => 'root';
    '/u01/11.2.0/grid/.ssh':
      ensure   => directory,
      mode     => '0640',
      owner    => 'grid',
      require  => [ File['/u01/11.2.0/grid'], User['grid'] ];
    '/u01/app/oraInventory':
      ensure   => directory,
      mode     => '0664',
      owner    => 'grid',
      group    => 'oinstall',
      require  => [ File['/u01/app'], User['grid'], Group['oinstall'] ];
  }

  # TODO: separate into oracledb::grid?
  k5login { '/u01/11.2.0/grid/.k5login':
    purge      => true,
    require    => File['/u01/11.2.0/grid'],
    principals => $oracledb::ntirety::ntiretyusers,
  }
}

class oracledb::rac::v11g inherits oracledb::v11g {
  Base::Sysctl['net.core.rmem_default'] { ensure => '4194304' }
  Base::Sysctl['net.core.rmem_max']     { ensure => '4194304' }
}

class oracledb::rac::root inherits user::root::sa-crc-plus-sa-non-crc {
  include oracledb::ntirety

  K5login['/root/.k5login'] {
    principals +> $oracledb::ntirety::ntiretyusers,
  }
}

# Override default for messages log to set group => dba.
class oracledb::rac::newsyslog inherits base::newsyslog {
  Base::Newsyslog::Config['messages'] {
    log_group => 'dba',
    require   => Group['dba'],
  }

}
