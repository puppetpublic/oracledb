#
# Handles an Oracle database system.  All of our Oracle databases are run
# essentially the same way and this module contains all the standard bits.

class oracledb {

  include base::libstdc
  include compatlibs
  include group::dba
  include oracledb::directories
  include oracledb::oracleusers
  include oracledb::packages
  include user::oracle

  if ($::operatingsystem != 'redhat') {
    fail ('Oracle only runs on Red Hat!')
  }

  case $::lsbmajdistrelease {
    ## rhel4
    '4': {
      package { [
        'glibc-kernheaders',
        'gnome-libs',
        'openmotif21',
        'pdksh',
        'xorg-x11-deprecated-libs',
        'xscreensaver',
        ]: ensure => present;
      }

      # Old workaround for up2date i386 arch packages on x86_64.
      exec {
        ' oracle 64 bit server required compat-db':
          command => 'up2date --arch=i386 compat-db',
          unless  => "rpm -q --qf \"%{ARCH}\n\" compat-db | grep -q i386";
        'oracle 64bit server required glibc-devel':
          command => 'up2date --arch=i386 glibc-devel',
          unless  => "rpm -q --qf \"%{ARCH}\n\" glibc-devel | grep -q i386";
        'oracle 64bit server required libaio':
          command => 'up2date --arch=i386 libaio',
          unless  => "rpm -q --qf \"%{ARCH}\n\" libaio | grep -q i386";
      }

      base::sysctl { 'net.ipv4.ip_local_port_range':  ensure  => '1024 65000' }

      # Override limits.conf to increase stack size and nofiles.
      if ($::architecture == 'x86_64') {
        $limitsfile = 'limits.conf.x86_64'
      } else {
        $limitsfile = 'limits.conf'
      }
      file { '/etc/security/limits.conf':
        source => "puppet:///modules/oracledb/etc/$limitsfile",
      }
    }
    ## rhel5
    '5': {
      include oracledb::rhel5

      file {'/etc/security/limits.conf':
        source =>  'puppet:///modules/oracledb/etc/limits.conf.rhel5'
      }
      base::sysctl { 'net.ipv4.ip_local_port_range':  ensure  => '9000 65500' }
    }
    default: {
      # TODO: confirm EL6 is supported, and if so, what package and kernel
      # params are needed.
    }
  }

  # Email to oracle user should go to Ntirety.
  base::postfix::recipient { 'oracle@stanford.edu':
    ensure => 'stanford@ntirety.com';
  }

  base::sysctl {
    'kernel.shmmax':         ensure => '4294967295';
    'kernel.sem':            ensure => '250 32000 100 128';
    'kernel.shmall':         ensure => '2097152';
    'kernel.shmmni':         ensure => '4096';
    'fs.file-max':           ensure => '65536';
    'net.core.rmem_default': ensure => '262144';
    'net.core.rmem_max':     ensure => '262144';
    'net.core.wmem_default': ensure => '262144';
    'net.core.wmem_max':     ensure => '262144';
  }

  base::iptables::rule { 'oracle-listeners':
    protocol => 'tcp',
    source   => [ '171.64.0.0/14',
                  '172.24.0.0/14',
                  '192.168.220.96/19',
                  '192.168.15.0/24'],
    port     => ['1533','1534','1535','1568','1542'],
  }

  # init script and filter-syslog rules for oracle
  if ( $::lsbmajdistrelease == 6 ) {
    $oracleinit = 'oracle.init.el6'
   } else  {
    $oracleinit = 'oracle.init'
  }
  file {
    '/etc/init.d/oracle':
      source => "puppet:///modules/oracledb/$oracleinit",
      mode   => '0755';
    '/etc/filter-syslog/oracle':
      source => 'puppet:///modules/oracledb/filter-syslog';
  }

  service { 'oracle':
    enable  => true,
    require => File['/etc/init.d/oracle'],
  }

}

