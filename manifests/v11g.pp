#
#  This is for rhel5 oracle11g

# TODO: should be handled via parameterized class
class oracledb::v11g inherits oracledb  {
  if ( $::operatingsystem != 'redhat') {
    fail 'Oracle only runs on Red Hat!'
  }

  base::sysctl {'fs.aio-max-nr':      ensure => 1048576 }
  Base::Sysctl['net.core.wmem_max'] { ensure => 1048576 }
  Base::Sysctl['net.core.rmem_max'] { ensure => 4194304 }
  Base::Sysctl['fs.file-max']       { ensure => 6815744 }

  package { [
    'openmotif',
    'elfutils-libelf-devel',
    'libaio-devel',
      ]:
        ensure => installed,
  }

  case $::lsbmajdistrelease  {
    '6' : {
      package { [
        # Ntirety required packages
        'compat-libstdc++-33.i686',
        'glibc-devel.i686',
        'ksh',
        'libaio.i686',
        'libstdc++.i686',
        'numactl-devel',
        'xorg-x11-utils',
          ]:
            ensure => installed,
      }
      base::sysctl {'net.ipv4.ip_local_port_range':
        ensure  => '9000 65500'
      }
      file {'/etc/security/limits.conf':
        source => 'puppet:///modules/oracledb/etc/limits.conf.rhel6'
      }
    }
    '5' : {
      package { [
        'unixODBC',
        'unixODBC-devel', ]:
          ensure => installed,
      }

      Base::Sysctl['net.core.rmem_default'] { ensure => '4194304' }
      Base::Sysctl['net.ipv4.ip_local_port_range'] {
        ensure => '9000 65500'
      }
    }
    '4': {
      package { 'xorg-x11':   ensure => present }
      Base::Sysctl['net.ipv4.ip_local_port_range'] {
        ensure  => '9000 65500'
      }
    }
    default: {
      fail 'Unrecognized RHEL release!'
    }
  }
}
