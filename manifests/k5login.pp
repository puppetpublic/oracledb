# class to handle oracle user .k5logins for all Oracle DBA scenarios

# this is a hack for oracledb::k5login::all
class oracledb::k5login {

  include oracledb::ntirety

  $allusers = $oracledb::as::asusers

}

class oracledb::k5login::all inherits oracledb::k5login {

  $allusers += $oracledb::ntirety::ntiretyusers

  k5login {
    '/home/emagent/.k5login':
      purge      => true,
      require    => [User[emagent], File['/home/emagent']],
      principals => $oracledb::as::asusers;
    '/opt/ntirety/.k5login':
      purge      => true,
      require    => [User[ntirety], File['/opt/ntirety']],
      principals => $oracledb::ntirety::ntiretyusers;
    '/u01/app/oracle/.k5login':
      purge      => true,
      mode       => 664,
      require    => [User[oracle], File['/u01/app/oracle']],
      principals => $allusers;
  }

}

class oracledb::k5login::as {

  k5login {
    '/u01/app/oracle/.k5login':
      purge      => true,
      mode       => 664,
      require    => [User[oracle], File['/u01/app/oracle']],
      principals => $oracledb::as::asusers;
    '/home/emagent/.k5login':
      purge      => true,
      require    => [User[emagent], File['/home/emagent']],
      principals => $oracledb::as::asusers;
  }

}

class oracledb::k5login::ntirety {

  k5login {
    '/opt/ntirety/.k5login':
      purge      => true,
      require    => [User['ntirety'], File['/opt/ntirety']],
      principals => $oracledb::ntirety::ntiretyusers;
    '/u01/app/oracle/.k5login':
      purge      => true,
      mode       => 664,
      require    => File['/u01/app/oracle'],
      principals => $oracledb::ntirety::ntiretyusers;
  }

}

