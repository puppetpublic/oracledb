#
# Set up a Tivoli backup client.
## thie is rhel6 86_64 for tpdo versin 6.3 or later
class oracledb::tivoli {
  include compatlibs
  include base::libstdc

  package { [
    'TIVsm-API64',
    'TIVsm-BA'
    ]:
      ensure  => installed;
  }

  file {
    '/opt/tivoli/tsm/client/ba/bin/dsm.sys':
      ensure  => link,
      target  => '/etc/tivoli/dsm.sys',
      require => [ Package['TIVsm-API64'], Package['TIVsm-BA'] ];
    '/opt/tivoli/tsm/client/ba/bin/dsm.opt':
      ensure  => link,
      target  => '/etc/tivoli/dsm.opt',
      require => [ Package['TIVsm-API64'], Package['TIVsm-BA'] ];
    '/opt/tivoli/tsm/client/ba/bin/inclexcl':
      ensure  => link,
      target  => '/etc/tivoli/inclexcl',
      require => [ Package['TIVsm-API64'], Package['TIVsm-BA'] ];
  }
  # This is required for RMAN backups.  Add dsm.sys under api.
  file { '/opt/tivoli/tsm/client/api/bin64/dsm.sys':
    ensure  => link,
    target  => '/etc/tivoli/dsm.sys',
    require => [ Package['TIVsm-API64'], Package['TIVsm-BA'] ],
  }

  # Install the init script.
  file { '/etc/init.d/dsmc':
    source => 'puppet:///modules/tivoli_client/init.dsmc.RedHat',
    mode   => '0775',
    notify => Exec['add dsmc'],
  }
  exec { 'add dsmc':
    command => '/sbin/chkconfig --add dsmc',
    require => File['/etc/init.d/dsmc'],
    unless  => '/sbin/chkconfig --list dsmc',
  }

  tivoli_client::inclexcl { $::fqdn: ensure => present }

    # Ensure dsmc is running.
  service { 'dsmc':
    ensure    => running,
    require   => File['/etc/init.d/dsmc'],
    hasstatus => false,
    status    => 'pidof dsmc || test ! -f /etc/adsm/TSM.PWD',
  }

  # Make sure the /etc/tivoli directory is there.
  file { '/etc/tivoli': ensure => directory }

  # Call the defintion file.
  tivoli_client::config { $::fqdn: nodename => $::hostname }
}
