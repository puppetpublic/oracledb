#

class oracledb::rhel5 inherits oracledb {
  if (! ( $::operatingsystem == 'redhat' and $::lsbmajdistrelease == '5' )) {
    fail 'Must be running RHEL5 to use oracledb::rhel5 class'
  }

  package {
    [ 'gnome-screensaver',
      'libgnome',
      'kernel-headers',
      'libFS',
      'libgomp',
      'pdksh',
      'xorg-x11-utils', ]:
        ensure => present;
  }
  # TODO: verify if you can use <name>.ARCH instead of exec.
  exec {
    ' R5.5 oracle 64 bit server required compat-db ':
      command => 'yum -y install compat-db.i386 ',
      unless  => 'rpm -q --qf \'%{ARCH}\n\' compat-db | grep -q i386';
    ' R5.5  oracle 64bit server required glibc-devel ':
      command => 'yum -y install glibc-devel.i386 ',
      unless  => 'rpm -q --qf \'%{ARCH}\n\' glibc-devel | grep -q i386';
    ' R5.5  oracle 64bit server required libaio':
      command => 'yum -y install libaio.i386',
      unless  => 'rpm -q --qf \'%{ARCH}\n\' libaio | grep -q i386';
    ' R5.5 oracle 64 bit server required libxTst ':
      command => 'yum -y install libXtst.i386 ',
      unless  => 'rpm -q --qf \'%{ARCH}\n\' libXtst | grep -q i386';
  }

  # setup oracle sources for ASM packages (needed for RAC)
  file { '/etc/yum.repos.d/oracle.repo':
    source => 'puppet:///modules/base/rpm/oracle-EL5.repo',
  }
  base::rpm::import { 'oracle-rpmkey':
    url       => 'http://yum.stanford.edu/RPM-GPG-KEY-oracle-el5',
    signature => 'gpg-pubkey-1e5e0159-464d0428',
  }
}
