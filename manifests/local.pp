#
# Subclass of the oracle class to handle systems with no SAN disk.  This also
# requires moving users around a bit.

# TODO: look into using a parameterized class here instead of override
class oracledb::local::oracleuser inherits user::oracle {
  File['/u01'] { ensure => '/opt/u01' }
  file {
    [ '/opt/u01',
      '/opt/u01/app',
      '/opt/u01/app/oracle', ]:
        ensure => directory,
        owner  => 'oracle',
        group  => 'dba';
  }
}

class oracledb::local inherits oracledb::directories {
  include oracledb
  include oracledb::local::oracleuser
  
  File['/u02'] { ensure => '/opt/u02' }
  File['/u03'] { ensure => '/opt/u03' }
  file {
    [ '/opt/u01/app/rman',
      '/opt/u02',
      '/opt/u03', ]:
        ensure => directory,
        owner  => 'oracle',
        group  => 'dba',
        mode   => '0770',
  }
}
